#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char *word;
    char *hash;
};

int by_hash(const void *a, const void *b){
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa->hash, bb->hash);
}


int search_by_hash(const void *t, const void *e){
    char *tt = (char *)t;
    struct entry *ee = (struct entry *)e;
    return strcmp(tt, ee->hash);
}




// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    
    int lines = 50;
    
    struct entry * words = malloc(lines * sizeof(struct entry));
    
    FILE *d = fopen(filename, "r");
    if(!d){
    	fprintf(stderr, "Cant open %s to read\n", filename);
    	exit(3);
	}
    char line[1000];
    int count = 0;
    while(fgets(line, 1000, d) != NULL){
        
        if(count == lines){
            
            lines += 50;
            words = realloc(words, lines * sizeof(struct entry));
        }
        
        char *word = malloc(strlen(line) * sizeof(char));
        line[strlen(line)-1] = '\0';
        
        strcpy(word, line);
        words[count].word = word;
        words[count].hash = md5(line, strlen(line));
        count++;
    }
    fclose(d);
    
    *size = count;
    return words;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int size = 0;
    struct entry *dict = read_dictionary(argv[2], &size);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, size, sizeof(struct entry), by_hash);
    
    // TODO
    // Open the hash file for reading.
    FILE *h = fopen(argv[1], "r");
    if(!h){
    	fprintf(stderr, "Cant open %s to read\n", argv[1]);
    	exit(3);
	}

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char line[100];
    while(fgets(line, 1000, h) != NULL){
        line[strlen(line)-1] = '\0';
        struct entry *found = bsearch(line, dict, size, sizeof(struct entry), search_by_hash);
        if(found){
            printf("Found %s %s\n", found->word, found->hash);
        }
    }
    
    for(int i=0; i<size; i++){
        free(dict[i].word);
        free(dict[i].hash);
    }

    free(dict);
    fclose(h);
}
